defmodule BankApi.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :value, :decimal
      add :transaction_type, :string
      add :user_from_id, references(:users, on_delete: :nothing, type: :binary_id), null: false
      add :user_to_id, references(:users, on_delete: :nothing, type: :binary_id), null: true

      timestamps()
    end

    create index(:transactions, [:user_from_id])
  end
end
