FROM elixir:latest

RUN apt-get update && \
  apt-get install -y postgresql-client

RUN mkdir /app
COPY . /app
WORKDIR /app

ENV SECRET_KEY_BASE=${SECRET_KEY_BASE}
RUN export SECRET_KEY_BASE

RUN MIX_ENV=dev

RUN mix local.rebar --force
RUN mix local.hex --force
RUN mix deps.get
RUN mix do compile

RUN chmod +x /app/entrypoint.sh
CMD ["/app/entrypoint.sh"]
