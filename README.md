# BankApi

Projeto desenvolvido para resolver o Desafio Backend Stone - API de Banking

## Pré-requisitos:  
- [Docker](https://www.docker.com/)  
- [Docker Compose](https://docs.docker.com/compose/)

## Executando
Faça o build das imagens e suba o projeto com o comando:
```
  $ docker-compose up --build
```

Você pode adicionar um outro valor a `SECRET_KEY_BASE` se quiser seguindo os passos abaixo:
- Acesse o container na api `$ docker exec -it api bash`
- Gere uma `SECRET_KEY_BASE` com o comando `$ mix phx.gen.secret`
- Copie e cole o valor no `.env`

O projeto estará disponível em: [http://localhost:4000](http://localhost:4000)

## Rodando os tests
Para rodar os testes basta executar `mix test` no container da api:
```
  $ docker exec -it api bash
  root@f1fdbb405f95:/app# mix test
```

## Documentação da API
[Collections do postman](https://documenter.getpostman.com/view/9736746/SWLZfVaw).

- [POST] - Sign Up `/api/users/sign-up`
  - Body params
    ```
    {
      "user": {
        "name": "Rick Sanchez",
        "email": "rick.sanchez@greenportal.com",
        "password": "wabbalabbadubdub"
      }
    }
    ```
  - Response
    ```
    {
      "data": {
        "user": {
          "balance": 1000,
          "email": "rick.sanchez@greenportal.com",
          "id": "84eba2d7-368d-4737-9ba3-03b924f57c06",
          "name": "Rick Sanchez",
          "password_hash": "$2b$12$4upDUnLIHh17AZmFbF9pmuiHNivakC32Td2F75AkMFwXnCSEqagxm"
        }
      }
    }
    ```

- [POST] - Sign In `/api/users/sign-in`
  - Body params
    ```
    {
      "email": "rick.sanchez@greenportal.com",
      "password": "wabbalabbadubdub"
    }
    ```
  - Response
    ```
    {
      "data": {
        "user": {
          "email": "rick.sanchez@greenportal.com",
          "id": "84eba2d7-368d-4737-9ba3-03b924f57c06"
        }
      }
    }
    ```

- [POST] - Transfer `/api/transaction/transfer`
  - Headers: `Authorization`: Basic Auth
  - Body params
    ```
    {
      "transaction_type": "transfer",
      "value": "10",
      "user_to": "morty.smith@greenportal.com"
    }
    ```
  - Response
    ```
    {
      "data": {
        "transaction_type": "transfer",
        "user_from": {
          "user": {
            "balance": "990",
            "email": "rick.sanchez@greenportal.com",
            "id": "84eba2d7-368d-4737-9ba3-03b924f57c06",
            "name": "Rick Sanchez",
            "password_hash": "$2b$12$4upDUnLIHh17AZmFbF9pmuiHNivakC32Td2F75AkMFwXnCSEqagxm"
            }
        },
        "user_to": {
          "user": {
            "balance": "1010",
            "email": "morty.smith@greenportal.com",
            "id": "69c927eb-443f-473b-af27-dd6122820f0b",
            "name": "Morty Smith",
            "password_hash": "$2b$12$haWQlgzBP4E4qWtySxXr2eHqDG6222x09PahJQuesynvelJU5yBXu"
          }
        },
        "value": "10"
      }
    }
    ```

- [POST] - Withdraw `/api/transaction/withdraw`
  - Headers: `Authorization`: Basic Auth
  - Body params
    ```
    {	
      "value": "10",
      "transaction_type": "withdraw"
    }
    ```
  - Response
    ```
    {
      "data": {
        "transaction_type": "withdraw",
        "user_from": {
          "user": {
            "balance": "980",
            "email": "rick.sanchez@greenportal.com",
            "id": "84eba2d7-368d-4737-9ba3-03b924f57c06",
            "name": "Rick Sanchez",
            "password_hash": "$2b$12$4upDUnLIHh17AZmFbF9pmuiHNivakC32Td2F75AkMFwXnCSEqagxm"
          }
        },
        "user_to": null,
        "value": "10"
      }
    }
    ```
- [GET] - Backoffice `/api/transaction/backoffice`
  - Url params `filter=day|month|year|all`
    - Response `filter=day`
      ```
      [
        {
          "day": 4.0,
          "month": 1.0,
          "total": "20",
          "year": 2020.0
        }
      ]
      ```
    - Response `filter=month`
      ```
      [
        {
          "month": 1.0,
          "total": "20",
          "year": 2020.0
        }
      ]
      ```
    - Response `filter=year`
      ```
      [
        {
          "total": "20",
          "year": 2020.0
        }
      ]
      ```
    - Response `filter=all`
      ```
      [
        {
          "total": "20"
        }
      ]
      ```

## Deploy
O deploy foi feito seguindo essa documentação: https://gigalixir.readthedocs.io/en/latest/main.html#install-distillery-to-build-releases


Integrado ao [GitLab CI/CD](https://gitlab.com/rphl-snts/bank-api/pipelines), com 3 estágios de verificação e por fim o deploy para o [gigalixir](https://gigalixir.com/).

![pipeline](https://user-images.githubusercontent.com/18425056/71454524-0ca23980-2770-11ea-9e29-c1491791ed9f.png)

Depois que o deploy for concluido, faça o login no gigalixir via linha de comando: `$ gigalixir login`
E rode as migrations: `$ gigalixir ps:migrate`

Accesse a API em https://bank-api.gigalixirapp.com/
