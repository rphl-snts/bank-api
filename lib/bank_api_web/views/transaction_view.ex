defmodule BankApiWeb.TransactionView do
  use BankApiWeb, :view
  alias BankApiWeb.TransactionView
  alias BankApiWeb.UserView

  def render("index.json", %{transactions: transactions}) do
    %{data: render_many(transactions, TransactionView, "transaction.json")}
  end

  def render("show.json", %{transaction: transaction}) do
    %{data: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("transaction.json", %{transaction: transaction}) do
    %{
      value: transaction.value,
      transaction_type: transaction.transaction_type,
      user_from: render_one(transaction.user_from, UserView, "user.json"),
      user_to: render_one(transaction.user_to, UserView, "user.json")
    }
  end
end
