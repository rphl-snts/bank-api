defmodule BankApiWeb.UserView do
  use BankApiWeb, :view
  alias BankApiWeb.UserView

  def render("sign_up.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("sign_in.json", %{user: user}) do
    %{
      data: %{
        user: %{
          id: user.id,
          email: user.email
        }
      }
    }
  end

  def render("user.json", %{user: user}) do
    %{
      user: %{
        id: user.id,
        email: user.email,
        name: user.name,
        password_hash: user.password_hash,
        balance: user.balance
      }
    }
  end
end
