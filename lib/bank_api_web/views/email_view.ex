defmodule BankApiWeb.EmailView do
  use BankApiWeb, :view

  def render("withdraw.json", %{transaction: transaction}) do
    new_balance =
      transaction.user_from.balance |> Decimal.add(transaction.value |> Decimal.minus())

    %{
      email: %{
        subject: "Successful withdrawal",
        body: %{
          value_withdrawn: transaction.value,
          new_balance: new_balance
        }
      }
    }
  end
end
