defmodule BankApiWeb.UserController do
  use BankApiWeb, :controller

  alias BankApi.Accounts
  alias BankApi.Accounts.User

  action_fallback BankApiWeb.FallbackController

  def sign_up(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params) do
      conn
      |> put_status(:created)
      |> render("sign_up.json", user: user)
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
    case BankApi.Accounts.authenticate_user(email, password) do
      {:ok, user} ->
        conn
        |> put_session(:current_user_id, user.id)
        |> put_status(:ok)
        |> put_view(BankApiWeb.UserView)
        |> render("sign_in.json", user: user)

      {:error, message} ->
        conn
        |> delete_session(:current_user_id)
        |> put_status(:unauthorized)
        |> put_view(BankApiWeb.ErrorView)
        |> render("401.json", message: message)
    end
  end
end
