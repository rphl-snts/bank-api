defmodule BankApiWeb.TransactionController do
  use BankApiWeb, :controller

  alias BankApi.Accounts
  alias BankApi.Accounts.Transaction

  action_fallback BankApiWeb.FallbackController

  def backoffice(conn, params) do
    filter = params["filter"] || nil
    transactions = Accounts.filter_transactions_by(filter)

    conn
    |> put_status(:ok)
    |> json(transactions)
  end

  def withdraw(conn, params) do
    current_user = get_session(conn, :current_user_id)
    params = params |> Map.put("user_from_id", current_user)

    case Accounts.create_transaction(params) do
      {:ok, %Transaction{} = transaction} ->
        transaction_response({:ok, transaction}, conn)

      {:error, reason} ->
        transaction_response({:error, reason}, conn)
    end
  end

  def transfer(conn, params) do
    current_user = get_session(conn, :current_user_id)
    user_to = Accounts.get_user_by_email(params["user_to"])

    params =
      params
      |> Map.put("user_from_id", current_user)
      |> Map.put("user_to_id", user_to.id)

    case Accounts.create_transaction(params) do
      {:ok, %Transaction{} = transaction} ->
        transaction_response({:ok, transaction}, conn)

      {:error, reason} ->
        transaction_response({:error, reason}, conn)
    end
  end

  defp transaction_response({:ok, transaction}, conn) do
    conn
    |> put_status(:created)
    |> render("show.json", transaction: transaction)
  end

  defp transaction_response({:error, reason}, conn) do
    conn
    |> put_status(:bad_request)
    |> put_view(BankApiWeb.ErrorView)
    |> render("400.json", message: reason)
  end
end
