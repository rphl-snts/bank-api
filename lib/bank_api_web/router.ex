defmodule BankApiWeb.Router do
  use BankApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  pipeline :api_auth do
    plug :ensure_authenticated
  end

  scope "/api" do
    pipe_through :api

    scope "/users", BankApiWeb do
      post "/sign-up", UserController, :sign_up
      post "/sign-in", UserController, :sign_in
    end

    scope "/transaction", BankApiWeb do
      get "/backoffice", TransactionController, :backoffice

      pipe_through :api_auth
      post "/withdraw", TransactionController, :withdraw
      post "/transfer", TransactionController, :transfer
    end
  end

  # Plug function
  defp ensure_authenticated(conn, _opts) do
    current_user_id = get_session(conn, :current_user_id)

    if current_user_id do
      conn
    else
      conn
      |> put_status(:unauthorized)
      |> put_view(BankApiWeb.ErrorView)
      |> render("401.json", message: "Unauthenticated user")
      |> halt()
    end
  end
end
