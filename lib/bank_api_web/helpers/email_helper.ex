defmodule BankApiWeb.Helpers.EmailHelper do
  @moduledoc false
  require Logger

  alias BankApiWeb.EmailView

  def send_email(transaction) do
    if transaction.transaction_type == "withdraw" do
      email_info = EmailView.render("withdraw.json", transaction: transaction)
      Logger.info(Kernel.inspect(email_info))
    end
  end
end
