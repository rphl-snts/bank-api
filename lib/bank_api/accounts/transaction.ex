defmodule BankApi.Accounts.Transaction do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias BankApi.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "transactions" do
    field :value, :decimal
    field :transaction_type, :string
    belongs_to :user_from, User
    belongs_to :user_to, User

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [
      :value,
      :transaction_type,
      :user_from_id,
      :user_to_id
    ])
    |> validate_required([
      :value,
      :user_from_id,
      :transaction_type
    ])
  end
end
