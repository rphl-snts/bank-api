defmodule BankApi.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias BankApi.Accounts
  alias BankApi.Accounts.User
  alias BankApi.Repo
  alias BankApiWeb.Helpers.EmailHelper

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Gets a single user by email.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user_by_email(valid_email)
      {:ok, %User{}}

      iex> get_user!(invalid_email)
      ** (Ecto.NoResultsError)

  """
  def get_user_by_email(email) do
    Repo.get_by(User, email: email)
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
    Returns an authenticate user.

    ## Examples

      iex> authenticate_user(email, password)
      {:ok, %User{}}

      iex> authenticate_user(invalid_email, invalid_password)
      {:error, "Wrong email or password"}

  """
  def authenticate_user(email, password) do
    query = from(u in User, where: u.email == ^email)
    query |> Repo.one() |> verify_password(password)
  end

  defp verify_password(nil, _) do
    # Perform a dummy check to make user enumeration more difficult
    Bcrypt.no_user_verify()
    {:error, "Wrong email or password"}
  end

  defp verify_password(user, password) do
    if Bcrypt.verify_pass(password, user.password_hash) do
      {:ok, user}
    else
      {:error, "Wrong email or password"}
    end
  end

  alias BankApi.Accounts.Transaction
  alias BankApiWeb.Helpers.EmailHelper

  @doc """
  Filter transactions backoffice.
  """
  def filter_transactions_by("day"), do: Repo.all(transactions_by_day())
  def filter_transactions_by("month"), do: Repo.all(transactions_by_month())
  def filter_transactions_by("year"), do: Repo.all(transactions_by_year())
  def filter_transactions_by(_), do: Repo.all(all_transactions())

  @doc """
  Returns transactions filtered by day.

    ## Examples

        iex> filter_transactions_by("day")
        [
          {
            "day": 4.0,
            "month": 1.0,
            "total": "1004",
            "year": 2020.0
          }
        ]
  """
  def transactions_by_day do
    from t in Transaction,
      select: %{
        total: sum(t.value),
        day: fragment("date_part('day', ?)", t.inserted_at),
        month: fragment("date_part('month', ?)", t.inserted_at),
        year: fragment("date_part('year', ?)", t.inserted_at)
      },
      group_by: [
        fragment("date_part('day', ?)", t.inserted_at),
        fragment("date_part('month', ?)", t.inserted_at),
        fragment("date_part('year', ?)", t.inserted_at)
      ],
      order_by: [
        fragment("date_part('year', ?) ASC", t.inserted_at),
        fragment("date_part('month', ?) ASC", t.inserted_at),
        fragment("date_part('day', ?) ASC", t.inserted_at)
      ]
  end

  @doc """
  Returns transactions filtered by month.

    ## Examples

        iex> filter_transactions_by("month")
        [
          {
            "month": 1.0,
            "total": "1004",
            "year": 2020.0
          }
        ]
  """
  def transactions_by_month do
    from t in Transaction,
      select: %{
        total: sum(t.value),
        year: fragment("date_part('year', ?)", t.inserted_at),
        month: fragment("date_part('month', ?)", t.inserted_at)
      },
      group_by: [
        fragment("date_part('month', ?)", t.inserted_at),
        fragment("date_part('year', ?)", t.inserted_at)
      ],
      order_by: [
        fragment("date_part('year', ?) ASC", t.inserted_at),
        fragment("date_part('month', ?) ASC", t.inserted_at)
      ]
  end

  @doc """
  Returns transactions filtered by year.

    ## Examples

      iex> filter_transactions_by("year")
      [
        {
          "total": 1004,
          "year": 2020.0
        }
      ]
  """
  def transactions_by_year do
    from t in Transaction,
      select: %{
        total: sum(t.value),
        year: fragment("date_part('year', ?)", t.inserted_at)
      },
      group_by: [fragment("date_part('year', ?)", t.inserted_at)],
      order_by: [fragment("date_part('year', ?) ASC", t.inserted_at)]
  end

  @doc """
  Returns total of all transactions.

    ## Examples

      iex> filter_transactions_by(_)
      [
        {
            "total": "1004"
        }
      ]
  """
  def all_transactions do
    from t in Transaction,
      select: %{total: sum(t.value)}
  end

  @doc """
  Creates a transaction.

  ## Examples

      iex> create_transaction(valid_attrs)
      {:ok, %Transaction{}}

      iex> create_transaction(invalid_attrs)
      {:error, %Ecto.Changeset{}}

  """
  def create_transaction(attrs \\ %{}) do
    Repo.transaction(fn ->
      with {:ok, _} <- Decimal.parse(attrs["value"]),
           {:ok, transaction} <- %Transaction{} |> Transaction.changeset(attrs) |> Repo.insert() do
        transaction = Repo.preload(transaction, [:user_from, :user_to])

        case transaction.transaction_type do
          "withdraw" -> make_withdraw(transaction)
          "transfer" -> make_transfer(transaction)
        end

        Repo.preload(transaction, [:user_from, :user_to], force: true)
      else
        {:error, changeset} -> handle_transaction_error(changeset) |> Repo.rollback()
      end
    end)
  end

  @doc """
  Execute a withdraw and updating usrequire IExer balance.

  ## Examples

    iex> make_withdraw(valid_transaction)
    {:ok, %User{}}

    iex> make_withdraw(invalid_transaction)
    {:error, }
  """
  def make_withdraw(transaction) do
    case Decimal.positive?(transaction.value) do
      true ->
        user_from = transaction.user_from
        new_balance = user_from.balance |> Decimal.add(transaction.value |> Decimal.minus())

        case Decimal.positive?(new_balance) do
          true ->
            Accounts.update_user(user_from, %{balance: new_balance})
            EmailHelper.send_email(transaction)

          false ->
            Repo.rollback("Account cannot be negative")
        end

      false ->
        Repo.rollback("Value cannot be negative")
    end
  end

  @doc """
  Execute a transfer and updating user balance.

  ## Examples

    iex> make_transfer(valid_transaction)
    {:ok, %User{}}

    iex> make_transfer(invalid_transaction)
    {:error, }
  """
  def make_transfer(transaction) do
    case Decimal.positive?(transaction.value) do
      true ->
        make_withdraw(transaction)
        user_to = transaction.user_to
        new_balance = user_to.balance |> Decimal.add(transaction.value)

        case Decimal.positive?(new_balance) do
          true ->
            Accounts.update_user(user_to, %{balance: new_balance})

          false ->
            Repo.rollback("Account cannot be negative")
        end

      false ->
        Repo.rollback("Value cannot be negative")
    end
  end

  defp handle_transaction_error(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end
end
