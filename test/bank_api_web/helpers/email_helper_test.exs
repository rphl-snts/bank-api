defmodule BankApiWeb.Helpers.EmailHelperTest do
  use BankApiWeb.ConnCase

  alias BankApi.Accounts
  alias BankApiWeb.Helpers.EmailHelper

  describe "send email" do
    setup context do
      context
      |> Map.put(:transaction, insert(:transaction, transaction_type: "withdraw"))
    end

    test "logs email with account information", %{transaction: transaction} do
      user_from = Accounts.get_user!(transaction.user_from_id)
      transaction = Map.put(transaction, :user_from, user_from)

      assert :ok == EmailHelper.send_email(transaction)
    end
  end
end
