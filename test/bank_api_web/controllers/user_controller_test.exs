defmodule BankApiWeb.UserControllerTest do
  use BankApiWeb.ConnCase

  alias BankApi.Accounts
  alias Plug.Test

  @create_attrs %{
    name: "Morty Smith",
    email: "morty.smith@greenportal.com",
    password: "wabbalabadubdub"
  }

  @invalid_attrs %{
    name: nil,
    email: nil,
    password: nil
  }

  @current_user_attrs %{
    name: "Rick Sanchez",
    email: "rick.sanchez@greenportal.com",
    password: "wabbalabadubdub"
  }

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_attrs)
    user
  end

  def fixture(:current_user) do
    {:ok, current_user} = Accounts.create_user(@current_user_attrs)
    current_user
  end

  setup %{conn: conn} do
    {:ok, conn: conn, current_user: current_user} = setup_current_user(conn)
    {:ok, conn: put_req_header(conn, "accept", "application/json"), current_user: current_user}
  end

  describe "create user" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :sign_up), user: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]["user"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :sign_up), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "sign_in user" do
    test "renders user when user credentials are good", %{conn: conn, current_user: current_user} do
      conn =
        post(
          conn,
          Routes.user_path(conn, :sign_in, %{
            email: current_user.email,
            password: @current_user_attrs.password
          })
        )

      assert json_response(conn, 200)["data"] == %{
               "user" => %{
                 "id" => current_user.id,
                 "email" => current_user.email
               }
             }
    end

    test "renders errors when user credentials are bad", %{conn: conn} do
      conn =
        post(conn, Routes.user_path(conn, :sign_in, %{email: "non-existent email", password: ""}))

      assert json_response(conn, 401)["errors"] == %{"detail" => "Wrong email or password"}
    end
  end

  defp setup_current_user(conn) do
    current_user = fixture(:current_user)

    {:ok,
     conn: Test.init_test_session(conn, current_user_id: current_user.id),
     current_user: current_user}
  end
end
