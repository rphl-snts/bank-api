defmodule BankApiWeb.TransactionControllerTest do
  import Plug.Conn
  use BankApiWeb.ConnCase

  alias Plug.Test

  setup do
    sign_in(%{conn: build_conn()})
  end

  setup %{conn: conn} do
    {:ok, conn: conn, current_user: current_user} = setup_current_user(conn)
    {:ok, conn: put_req_header(conn, "accept", "application/json"), current_user: current_user}
  end

  describe "backoffice" do
    test "filter transaction by day", %{conn: conn} do
      conn = get(conn, Routes.transaction_path(conn, :backoffice), %{filter: "day"})
      assert conn.status == 200
    end

    test "filter transaction by month", %{conn: conn} do
      conn = get(conn, Routes.transaction_path(conn, :backoffice), %{filter: "mont"})
      assert conn.status == 200
    end

    test "filter transaction by year", %{conn: conn} do
      conn = get(conn, Routes.transaction_path(conn, :backoffice), %{filter: "year"})
      assert conn.status == 200
    end

    test "list all transactions", %{conn: conn} do
      conn = get(conn, Routes.transaction_path(conn, :backoffice), %{filter: "all"})
      assert conn.status == 200
    end
  end

  describe "create a transfer transaction" do
    test "renders transaction", %{conn: conn} do
      user_to = insert(:user)
      params = params_for(:transaction) |> Map.put("user_to", user_to.email)

      conn = post(conn, Routes.transaction_path(conn, :transfer), params)
      assert %{"transaction_type" => transaction_type} = json_response(conn, 201)["data"]
    end
  end

  describe "create a withdraw transaction" do
    test "renders transaction", %{conn: conn} do
      params = %{"value" => "10", "transaction_type" => "withdraw"}
      conn = post(conn, Routes.transaction_path(conn, :withdraw), params)
      assert %{"transaction_type" => transaction_type} = json_response(conn, 201)["data"]
    end
  end

  defp setup_current_user(conn) do
    current_user = insert(:user)

    {:ok,
     conn: Test.init_test_session(conn, current_user_id: current_user.id),
     current_user: current_user}
  end
end
