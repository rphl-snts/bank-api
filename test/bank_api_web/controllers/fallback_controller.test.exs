defmodule BankApiWeb.Api.FallbackControllerTest do
  use BankApiWeb.ConnCase, async: true

  alias BankApi.Accounts.User
  alias BankApiWeb.FallbackController

  test "call/2 with changeset", %{conn: conn} do
    response = FallbackController.call(conn, {:error, User.changeset(%User{}, %{})})

    assert Map.has_key?(json_response(response, 422)["errors"], "name")
  end

  test "call/2 with specific list of errors", %{conn: conn} do
    response = FallbackController.call(conn, ["first error", "second error"])

    assert json_response(response, 422) == %{"errors" => ["first error", "second error"]}
  end

  test "call/2 with specific tuple error", %{conn: conn} do
    response = FallbackController.call(conn, {:error, "specific error"})

    assert json_response(response, 422) == %{"errors" => ["specific error"]}
  end

  test "call/2 with string error", %{conn: conn} do
    response = FallbackController.call(conn, "an error")

    assert json_response(response, 422) == %{"errors" => ["an error"]}
  end

  test "call/2 with unknown error", %{conn: conn} do
    response = FallbackController.call(conn, nil)

    assert json_response(response, 422) == %{"errors" => ["Unknown Error"]}
  end
end
