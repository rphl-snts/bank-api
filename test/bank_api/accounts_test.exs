defmodule BankApi.AccountsTest do
  use BankApi.DataCase

  alias BankApi.Accounts

  describe "users" do
    alias BankApi.Accounts.User

    @valid_attrs %{
      name: "Darth Vader",
      email: "darth.vader@empire.com",
      password: "ilovemyfamily",
      balance: 1000
    }

    @update_attrs %{
      name: "Darth Vader",
      email: "darth.vader@empire.com",
      password: "ilovemyfamily",
      balance: 1050
    }

    @invalid_attrs %{
      name: nil,
      email: nil,
      password: nil,
      balance: nil
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      %{user | password: nil}
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "get_user_by_email/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user_by_email(user.email) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.name == "Darth Vader"
      assert user.email == "darth.vader@empire.com"
      assert user.password == "ilovemyfamily"
      assert user.balance == Decimal.new("1000")
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert user.balance == Decimal.new("1050")
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "authenticate_user/2 authenticates the user" do
      user = user_fixture()
      assert {:error, "Wrong email or password"} = Accounts.authenticate_user("wrong email", "")

      assert {:ok, authenticated_user} =
               Accounts.authenticate_user(user.email, @valid_attrs.password)

      assert %{user | password: nil} == authenticated_user
    end
  end

  describe "transactions" do
    alias BankApi.Accounts.Transaction

    @withdraw_valid_attrs %{
      "value" => "100",
      "transaction_type" => "withdraw",
      "user_from_id" => nil,
      "user_to_id" => nil
    }

    @transfer_valid_attrs %{
      "value" => "100",
      "transaction_type" => "transfer",
      "user_from_id" => nil,
      "user_to_id" => nil
    }

    @withdraw_invalid_attrs %{
      "value" => "5000",
      "transaction_type" => "withdraw",
      "user_from_id" => nil,
      "user_to_id" => nil
    }

    @transfer_invalid_attrs %{
      "value" => "5000",
      "transaction_type" => "transfer",
      "user_from_id" => nil,
      "user_to_id" => nil
    }

    @current_user_attrs %{
      "name" => "Morty Smith",
      "email" => "morty.smith@greenportal",
      "password" => "password",
      "balance" => 1000
    }

    def fixture(:current_user) do
      {:ok, current_user} = Accounts.create_user(@current_user_attrs)
      current_user
    end

    def transaction_fixture(attrs \\ %{}) do
      {:ok, transaction} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_transaction()

      transaction
    end

    test "filter_transactions_by/1 day" do
      transaction = insert(:transaction)

      transaction_filter_by_day = Accounts.filter_transactions_by("day")
      assert length(transaction_filter_by_day) == 1

      first = List.first(transaction_filter_by_day)
      assert first.day == transaction.inserted_at.day
      assert first.month == transaction.inserted_at.month
      assert first.year == transaction.inserted_at.year
      assert first.total == transaction.value
    end

    test "filter_transactions_by/1 month" do
      transaction = insert(:transaction)

      transaction_filter_by_month = Accounts.filter_transactions_by("month")
      assert length(transaction_filter_by_month) == 1

      first = List.first(transaction_filter_by_month)
      assert first.month == transaction.inserted_at.month
      assert first.year == transaction.inserted_at.year
      assert first.total == transaction.value
    end

    test "filter_transactions_by/1 year" do
      transaction = insert(:transaction)

      transaction_filter_by_year = Accounts.filter_transactions_by("year")
      assert length(transaction_filter_by_year) == 1

      first = List.first(transaction_filter_by_year)
      assert first.year == transaction.inserted_at.year
      assert first.total == transaction.value
    end

    test "all_transactions/1 all" do
      transaction = insert(:transaction)

      all_transactions = Accounts.filter_transactions_by("all")
      assert length(all_transactions) == 1

      first = List.first(all_transactions)
      assert first.total == transaction.value
    end

    test "create_transaction/1 withdraw" do
      user_from = insert(:user)
      withdraw_valid_attrs = @withdraw_valid_attrs |> Map.put("user_from_id", user_from.id)

      assert {:ok, %Transaction{} = transaction} =
               Accounts.create_transaction(withdraw_valid_attrs)

      assert transaction.value == Decimal.new("100")
      assert transaction.transaction_type == "withdraw"
    end

    test "create_transaction/1 withdraw without enough money" do
      user_from = insert(:user)
      withdraw_invalid_attrs = @withdraw_invalid_attrs |> Map.put("user_from_id", user_from.id)

      assert {:error, reason} = Accounts.create_transaction(withdraw_invalid_attrs)
      assert reason == "Account cannot be negative"
    end

    test "create_transaction/1 withdraw with negative value" do
      user_from = insert(:user)

      withdraw_invalid_attrs =
        @withdraw_invalid_attrs
        |> Map.put("user_from_id", user_from.id)
        |> Map.put("value", "-100")

      assert {:error, reason} = Accounts.create_transaction(withdraw_invalid_attrs)
      assert reason == "Value cannot be negative"
    end

    test "create_transaction/1 transfer" do
      user_from = insert(:user)
      user_to = insert(:user)

      transfer_valid_attrs =
        @transfer_valid_attrs
        |> Map.put("user_from_id", user_from.id)
        |> Map.put("user_to", user_to.email)
        |> Map.put("user_to_id", user_to.id)

      assert {:ok, %Transaction{} = transaction} =
               Accounts.create_transaction(transfer_valid_attrs)

      assert transaction.value == Decimal.new("100")
      assert transaction.transaction_type == "transfer"

      assert transaction.user_from.balance ==
               user_from.balance |> Decimal.add(transaction.value |> Decimal.minus())

      assert transaction.user_to.balance == user_to.balance |> Decimal.add(transaction.value)
    end

    test "create_transaction/1 transfer without enough money" do
      user_from = insert(:user)
      user_to = insert(:user)

      transfer_invalid_attrs =
        @transfer_invalid_attrs
        |> Map.put("user_from_id", user_from.id)
        |> Map.put("user_to", user_to.email)
        |> Map.put("user_to_id", user_to.id)

      assert {:error, reason} = Accounts.create_transaction(transfer_invalid_attrs)

      assert reason == "Account cannot be negative"
    end

    test "create_transaction/1 transfer with negative value" do
      user_from = insert(:user)
      user_to = insert(:user)

      transfer_invalid_attrs =
        @transfer_invalid_attrs
        |> Map.put("user_from_id", user_from.id)
        |> Map.put("user_to", user_to.email)
        |> Map.put("user_to_id", user_to.id)
        |> Map.put("value", "-100")

      assert {:error, reason} = Accounts.create_transaction(transfer_invalid_attrs)

      assert reason == "Value cannot be negative"
    end
  end
end
