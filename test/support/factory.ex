defmodule BankApi.Factory do
  @moduledoc "Test factories definition"

  use ExMachina.Ecto, repo: BankApi.Repo

  use BankApi.UserFactory
  use BankApi.TransactionFactory
end
