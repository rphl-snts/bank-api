defmodule BankApi.UserFactory do
  @moduledoc """
  Factory for User
  """

  alias BankApi.Accounts.User

  defmacro __using__(_opts) do
    quote do
      def user_factory do
        %User{
          name: Faker.Name.name(),
          email: Faker.Internet.email(),
          password: "password",
          balance: 1000
        }
      end

      def with_transaction(%User{} = user) do
        insert_pair(:transaction, user: user)
        user
      end
    end
  end
end
