defmodule BankApi.TransactionFactory do
  @moduledoc """
  Factory for Transaction
  """

  alias BankApi.Accounts.{Transaction, User}

  defmacro __using__(_opts) do
    quote do
      def transaction_factory do
        user_from = insert(:user)

        %Transaction{
          value: "100",
          transaction_type: Enum.random(["transfer", "withdraw"]),
          user_from_id: user_from.id,
          user_to_id: nil
        }
      end

      def with_user(%User{} = user) do
        insert_pair(:user_id, user: user.id)
        user
      end
    end
  end
end
